class HomeController < ApplicationController
  before_action :set_results

  def index
  end

  def results
  end

  private
    def set_results
      if params['search']
        request = RestClient.get("http://www.recipepuppy.com/api/?q=#{params['search']}")
        @results = JSON.parse(request.body)
      end
    end
end
